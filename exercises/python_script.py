# ---- imports 

import sys
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


# ----- path

report_folder = "./manuscript/"

# ---- data simulation

n = 100
X = np.random.normal(loc = 2, scale = 0.4, size=n)
error = np.random.normal(loc = 0, scale = 0.1, size=n)
intercept = 3
slope = -1
Y = intercept + X*slope + error


# ---- create dataframe

data_dict = {"X": X,
			 "Y": Y}

data_df = pd.DataFrame.from_dict(data_dict)

# ---- data plotting
fontsize = 16

fig, ax = plt.subplots(1,2,figsize = (17,8))

ax[0].hist(data_df["Y"], bins = 20)
ax[0].set_xlabel("Y", fontsize=fontsize)
ax[0].set_ylabel("counts", fontsize=fontsize)
ax[0].grid()

ax[1].scatter(data_df["X"], data_df["Y"], c = "purple", s = 90, marker = "*")
ax[1].grid()
ax[1].set_ylabel("Y", fontsize=fontsize)
ax[1].set_xlabel("X", fontsize=fontsize)

fig.suptitle("Exploratory analysis")

plt.savefig(report_folder + "/figures/fig_exploratory_analysis.pdf")