,
{ src: 'reveal.js/plugin/handwriting/handwriting.js' }
	],

keyboard: {
        67: function () { RevealHandWriting.colorCycle() },	// Change color with 'c'.
        46: function () { RevealHandWriting.clear() },	    // Clear drawn things with 'DEL'.
        68: function () { RevealHandWriting.toggleNotesCanvas() },	// Toggle to Canvas with 'd'.
        70: function () { RevealHandWriting.toggleChalkboard() }, // Toggle to Chalkboard with 'f'

}


